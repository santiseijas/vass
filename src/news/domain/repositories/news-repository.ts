import { NewsApiResponse } from '@news-domain';

export interface NewsRepository {
  getAllNews(): Promise<NewsApiResponse>;
}
