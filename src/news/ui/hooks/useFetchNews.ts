import { NewsService } from '@news-application';
import { Article } from '@news-domain';
import { ApiNewsRepository } from '@news-infraestructure';
import { useFetch } from '@shared-hooks';

export const useFetchNews = () => {
  const newsRepository = new ApiNewsRepository();
  const newsService = new NewsService(newsRepository);
  const fetchNews = async (): Promise<Article[]> => {
    const response = await newsService.getAllNews();
    const { articles } = response;
    return articles;
  };

  return useFetch<Article[]>(fetchNews);
};
