import { Article } from '@news-domain';
import { useMemo } from 'react';

export const useFilterSortArticles = (articles: Article[], searchQuery: string, orderBy: 'date' | 'title'): Article[] => {
  const filteredArticles = useMemo(() => {
    return articles.filter((article) => article.title.toLowerCase().includes(searchQuery.toLowerCase()));
  }, [articles, searchQuery]);

  const sortedArticles = useMemo(() => {
    const comparator = (a: Article, b: Article) => {
      if (orderBy === 'title') {
        return a.title.localeCompare(b.title);
      } else {
        return new Date(a.publishedAt).getTime() - new Date(b.publishedAt).getTime();
      }
    };

    return filteredArticles.sort(comparator);
  }, [filteredArticles, orderBy]);

  return sortedArticles;
};
