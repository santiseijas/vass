import { NavigationContainer } from '@react-navigation/native';
import { fireEvent, render } from '@testing-library/react-native';
import React from 'react';
import { useFetchNews } from '../../hooks/useFetchNews';
import { Main } from './main';

jest.mock('../../hooks/useFetchNews');

interface WrapperProps {
  children: any;
}

const Wrapper: React.FC<WrapperProps> = ({ children }) => <NavigationContainer>{children}</NavigationContainer>;

const articles = [
  { title: 'Test Article 1', url: 'url1', publishedAt: '2021-01-01' },
  { title: 'Test Article 2', url: 'url2', publishedAt: '2021-02-01' },
];

describe('<Main />', () => {
  it('show loader when data is loading', () => {
    const mockUseFetchNews = useFetchNews as jest.Mock;
    mockUseFetchNews.mockImplementation(() => ({
      data: null,
      isLoading: true,
      error: null,
    }));

    const { getByTestId } = render(<Main />);
    expect(getByTestId('loadingIndicator')).toBeTruthy();
  });

  it('show error', async () => {
    const errorMessage = 'Error fetching';
    const mockUseFetchNews = useFetchNews as jest.Mock;
    mockUseFetchNews.mockImplementation(() => ({
      data: null,
      isLoading: false,
      error: { message: errorMessage },
    }));

    const { findByText } = render(<Main />);
    const errorElement = await findByText(errorMessage);
    expect(errorElement).toBeTruthy();
  });

  it('render each article', () => {
    const mockUseFetchNews = useFetchNews as jest.Mock;
    mockUseFetchNews.mockImplementation(() => ({
      data: articles,
      isLoading: false,
      error: null,
    }));

    const { getByText } = render(<Main />, { wrapper: Wrapper });

    articles.forEach((article) => {
      expect(getByText(article.title)).toBeTruthy();
    });
  });
  it('filter articles', () => {
    const mockUseFetchNews = useFetchNews as jest.Mock;
    mockUseFetchNews.mockImplementation(() => ({
      data: articles,
      isLoading: false,
      error: null,
    }));

    const { getByPlaceholderText, getByText, queryByText } = render(<Main />, { wrapper: Wrapper });

    const searchBar = getByPlaceholderText('Search by title');
    fireEvent.changeText(searchBar, 'Test Article 1');

    expect(getByText('Test Article 1')).toBeTruthy();
    expect(queryByText('Test Article 2')).toBeNull();
  });
});
