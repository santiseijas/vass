import { spacing } from '@shared-theme';
import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: spacing.sm,
  },
  searchBar: {
    marginVertical: spacing.md,
  },
  orderButtons: {
    flexDirection: 'row',
    marginBottom: spacing.md,
    gap: spacing.md,
  },
});
