import { Article } from '@news-domain';
import { CustomText, NoResults, OrderButton, SearchBar } from '@shared-components';
import { colors, spacing } from '@shared-theme';
import React, { useState } from 'react';
import { ActivityIndicator, FlatList, SafeAreaView, View } from 'react-native';
import { ArticleCard } from '../../components/article-card';
import { useFetchNews, useFilterSortArticles } from '../../hooks';
import { styles } from './styles';

export const Main = () => {
  const { data, isLoading, error } = useFetchNews();
  const [searchQuery, setSearchQuery] = useState<string>('');
  const [orderBy, setOrderBy] = useState<'date' | 'title'>('date');

  const displayedArticles = useFilterSortArticles(data || [], searchQuery, orderBy);

  return (
    <SafeAreaView style={styles.container}>
      {isLoading && <ActivityIndicator size='large' testID='loadingIndicator' />}
      {error && (
        <CustomText fontSize={spacing.xl} color={colors.error}>
          {error.message}
        </CustomText>
      )}
      {!isLoading && !error && (
        <>
          <View style={styles.searchBar}>
            <SearchBar searchQuery={searchQuery} setSearchQuery={setSearchQuery} placeholder='Search by title' />
          </View>
          <View style={styles.orderButtons}>
            <OrderButton orderBy='title' onSetOrder={setOrderBy} />
            <OrderButton orderBy='date' onSetOrder={setOrderBy} />
          </View>

          {displayedArticles.length > 0 ? (
            <FlatList
              data={displayedArticles}
              renderItem={({ item }: { item: Article }) => <ArticleCard item={item} />}
              keyExtractor={(item) => item.url}
              showsVerticalScrollIndicator={false}
            />
          ) : (
            searchQuery && <NoResults />
          )}
        </>
      )}
    </SafeAreaView>
  );
};
