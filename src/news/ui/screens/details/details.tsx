import { RouteProp } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { CustomText } from '@shared-components';
import { colors, spacing } from '@shared-theme';
import { formatDateToLocaleString } from '@shared-utils';
import { RootStackParamList } from 'App';
import React, { useCallback } from 'react';
import { Image, Linking, ScrollView, View } from 'react-native';
import { styles } from './styles';

interface DetailsProps {
  route: RouteProp<RootStackParamList, 'Details'>;
  navigation: NativeStackNavigationProp<RootStackParamList, 'Details'>;
}
export const Details: React.FC<DetailsProps> = ({ route }) => {
  const { item } = route.params;
  const { title, author, description, publishedAt, urlToImage, url } = item;

  const handlePress = useCallback(() => {
    Linking.openURL(url);
  }, [url]);

  return (
    <View style={styles.container}>
      <Image
        style={styles.image}
        source={{
          uri: urlToImage || '',
        }}
      />
      <View style={styles.content}>
        <ScrollView contentContainerStyle={styles.scrollview}>
          <CustomText style={styles.title} fontSize={spacing.xl} fontWeight='bold' color={colors.textSecondary}>
            {title}
          </CustomText>
          <CustomText style={styles.author} fontSize={spacing.md} color={colors.textSecondary}>
            {author}
          </CustomText>
          <CustomText style={styles.date} fontSize={spacing.md} color={colors.textSecondary}>
            {formatDateToLocaleString(publishedAt)}
          </CustomText>
          <CustomText fontSize={spacing.md} color={colors.primary} onPress={handlePress} style={styles.link}>
            Read full article
          </CustomText>
          <CustomText fontSize={spacing.md} color={colors.textSecondary}>
            {description}
          </CustomText>
        </ScrollView>
      </View>
    </View>
  );
};
