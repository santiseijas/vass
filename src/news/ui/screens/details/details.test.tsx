import { RouteProp } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { render } from '@testing-library/react-native';
import { RootStackParamList } from 'App';
import React from 'react';
import { Details } from './details';

jest.mock('@shared-utils', () => ({
  formatDateToLocaleString: jest.fn().mockImplementation((date) => 'formatedDate'),
}));

const mockNavigation = {
  navigate: jest.fn(),
} as unknown as NativeStackNavigationProp<RootStackParamList, 'Details'>;

let route: RouteProp<RootStackParamList, 'Details'> = {
  key: 'some-key',
  name: 'Details',
  params: {
    item: {
      source: {
        id: '1',
        name: 'nameTest',
      },
      author: 'autorTest',
      title: 'titleTest',
      description: 'descriptionTest',
      url: 'urlTest',
      urlToImage: 'urlToImageTest',
      publishedAt: 'publishedAtTest',
      content: 'contentTest',
    },
  },
};

describe('Details Component', () => {
  it('renders the details correctly', () => {
    const { getByText } = render(<Details navigation={mockNavigation} route={route} />);

    expect(getByText('titleTest')).toBeTruthy();
    expect(getByText('autorTest')).toBeTruthy();
    expect(getByText('descriptionTest')).toBeTruthy();
    expect(getByText('formatedDate')).toBeTruthy();
  });
});
