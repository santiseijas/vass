import { colors, spacing } from '@shared-theme';
import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    height: '50%',
    resizeMode: 'cover',
  },
  content: {
    borderTopLeftRadius: spacing.lg,
    borderTopRightRadius: spacing.lg,
    backgroundColor: colors.background,
    marginTop: -spacing.lg,
    padding: spacing.md,
  },
  scrollview: {
    flexGrow: 1,
    height: '80%',
  },
  title: {
    marginBottom: spacing.md,
  },
  author: {
    marginBottom: spacing.xxs,
  },
  date: {},
  link: {
    marginBottom: spacing.md,
    textDecorationLine: 'underline',
  },
});
