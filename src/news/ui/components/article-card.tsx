import { Article } from '@news-domain';
import { useNavigation } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { CustomText } from '@shared-components';
import { colors, spacing } from '@shared-theme';
import { RootStackParamList } from 'App';
import React from 'react';
import { Image, StyleSheet, TouchableOpacity, View } from 'react-native';

interface ArticleCardProps {
  item: Article;
}

export const ArticleCard: React.FC<ArticleCardProps> = ({ item }) => {
  const navigation = useNavigation<NativeStackNavigationProp<RootStackParamList>>();

  const navigateToDetails = () => {
    navigation.navigate('Details', { item });
  };

  return (
    <TouchableOpacity style={styles.container} onPress={navigateToDetails} testID='articleTouchableOpacity'>
      <Image
        style={styles.image}
        source={{
          uri: item.urlToImage || '',
        }}
        testID='articleImage'
      />
      <View style={styles.textOverlay}>
        <CustomText fontSize={spacing.md} fontWeight='bold' color={colors.background}>
          {item.title}
        </CustomText>
        <CustomText fontSize={spacing.sm} color={colors.background} numberOfLines={2}>
          {item.description}
        </CustomText>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    borderRadius: spacing.xs,
    overflow: 'hidden',
    marginBottom: spacing.xs,
    elevation: 4,
    shadowColor: colors.border,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 6,
    shadowOpacity: 0.3,
  },
  image: {
    height: spacing.xxxxxl * 8,
    width: '100%',
    resizeMode: 'cover',
  },
  textOverlay: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    padding: spacing.sm,
    backgroundColor: 'rgba(0, 0, 0, 0.4)',
  },
});
