import { NavigationContainer } from '@react-navigation/native';
import { fireEvent, render } from '@testing-library/react-native';
import React from 'react';
import { ArticleCard } from './article-card';

const mockNavigate = jest.fn();

jest.mock('@react-navigation/native', () => {
  const actualNav = jest.requireActual('@react-navigation/native');
  return {
    ...actualNav,
    useNavigation: () => ({
      navigate: mockNavigate,
    }),
  };
});

describe('ArticleCard', () => {
  const item = {
    source: {
      id: '1',
      name: 'nameTest',
    },
    author: 'autorTest',
    title: 'titleTest',
    description: 'descriptionTest',
    url: 'urlTest',
    urlToImage: 'urlToImageTest',
    publishedAt: 'publishedAtTest',
    content: 'contentTest',
  };

  it('renders correctly', () => {
    const { getByText, getByTestId } = render(<ArticleCard item={item} />);

    expect(getByText(item.title)).toBeTruthy();
    expect(getByText(item.description)).toBeTruthy();
    expect(getByTestId('articleImage').props.source.uri).toBe(item.urlToImage);
  });

  it('navigates to details on press', () => {
    const { getByTestId } = render(
      <NavigationContainer>
        <ArticleCard item={item} />
      </NavigationContainer>
    );

    const touchableOpacity = getByTestId('articleTouchableOpacity');
    fireEvent.press(touchableOpacity);

    expect(mockNavigate).toHaveBeenCalledWith('Details', { item });
  });
});
