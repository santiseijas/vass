import { NewsApiResponse, NewsRepository } from '@news-domain';
import { mapResponseToNewsApiResponse } from '../mappers';

export class ApiNewsRepository implements NewsRepository {
  async getAllNews(): Promise<NewsApiResponse> {
    const url = `https://saurav.tech/NewsAPI/everything/cnn.json`;
    try {
      const response = await fetch(url, { method: 'GET' });
      if (!response.ok) {
        const responseError = await response.json();
        throw new Error(`Error: ${responseError.errors[0]}`);
      }
      const data: NewsApiResponse = await response.json();
      return mapResponseToNewsApiResponse(data);
    } catch (e) {
      console.error(e);
      throw new Error('Failed to fetch news');
    }
  }
}
