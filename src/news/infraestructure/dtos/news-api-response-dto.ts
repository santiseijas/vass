import { ArticleDTO } from '@news-infraestructure';

export interface NewsApiResponseDTO {
  status: string;
  totalResults: number;
  articles: ArticleDTO[];
}
