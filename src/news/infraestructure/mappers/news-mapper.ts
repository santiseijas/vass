import { Article, NewsApiResponse } from '@news-domain';
import { NewsApiResponseDTO } from '../dtos';

export const mapResponseToNewsApiResponse = (rawData: NewsApiResponseDTO): NewsApiResponse => {
  return {
    status: rawData.status || '',
    totalResults: rawData.totalResults,
    articles: rawData.articles.map((article: Article) => ({
      source: {
        id: article.source.id,
        name: article.source.name,
      },
      author: article.author,
      title: article.title,
      description: article.description,
      url: article.url,
      urlToImage: article.urlToImage,
      publishedAt: article.publishedAt,
      content: article.content,
    })),
  };
};
