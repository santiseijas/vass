import { NewsApiResponse, NewsRepository } from '@news-domain';

export class NewsService {
  constructor(private _repository: NewsRepository) {}
  async getAllNews(): Promise<NewsApiResponse> {
    return this._repository.getAllNews();
  }
}
