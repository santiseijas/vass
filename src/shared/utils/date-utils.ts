export const formatDateToLocaleString = (date: string): string => {
  return new Date(date).toLocaleString();
};
