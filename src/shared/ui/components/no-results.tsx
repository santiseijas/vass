import { colors, spacing } from '@shared-theme';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { CustomText } from './custom-text';

export const NoResults = () => {
  return (
    <View style={styles.container}>
      <CustomText fontSize={spacing.xl} color={colors.error} fontWeight='bold'>
        No results found
      </CustomText>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: spacing.lg,
  },
  text: {
    fontSize: spacing.lg,
    color: colors.textSecondary,
  },
});
