import React from 'react';
import { Image, StyleSheet } from 'react-native';

export const Header = () => {
  return <Image style={styles.logo} source={require('./../../../../assets/header.jpeg')} resizeMode='contain' />;
};

const styles = StyleSheet.create({
  logo: {
    width: 200,
    height: 40,
  },
});
