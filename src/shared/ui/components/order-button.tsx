import { colors, spacing } from '@shared-theme';
import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { CustomText } from './custom-text';

interface OrderButtonProps {
  orderBy: 'date' | 'title';
  onSetOrder: (orderBy: 'date' | 'title') => void;
}

export const OrderButton: React.FC<OrderButtonProps> = ({ orderBy, onSetOrder }) => {
  return (
    <TouchableOpacity onPress={() => onSetOrder(orderBy)} style={styles.button}>
      <CustomText fontSize={spacing.sm} color={colors.background} fontWeight='bold'>
        Order by {orderBy}
      </CustomText>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    backgroundColor: colors.primary,
    padding: spacing.sm,
    borderRadius: spacing.xl,
    alignSelf: 'center',
  },
});
