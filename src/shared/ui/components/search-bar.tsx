import { colors, spacing } from '@shared-theme';
import React, { FC } from 'react';
import { StyleSheet, TextInput } from 'react-native';

interface SearchBarProps {
  searchQuery: string;
  setSearchQuery: (query: string) => void;
  placeholder: string;
}

export const SearchBar: FC<SearchBarProps> = ({ searchQuery, setSearchQuery, placeholder }) => {
  return <TextInput style={styles.searchInput} placeholder={placeholder} value={searchQuery} onChangeText={setSearchQuery} />;
};

const styles = StyleSheet.create({
  searchInput: {
    padding: spacing.sm,
    borderRadius: spacing.xl,
    borderWidth: 1,
    borderColor: colors.border,
  },
});
