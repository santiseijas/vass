import React from 'react';
import { Text, TextProps } from 'react-native';

interface CustomTextProps extends TextProps {
  fontSize?: number;
  fontWeight?: 'normal' | 'bold' | '100' | '200' | '300' | '400' | '500' | '600' | '700' | '800' | '900';
  color?: string;
}

export const CustomText: React.FC<CustomTextProps> = ({ children, fontSize, fontWeight, color, style, ...rest }) => {
  return (
    <Text style={[{ fontSize, fontWeight, color }, style]} {...rest} testID='customText'>
      {children}
    </Text>
  );
};
