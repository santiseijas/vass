import { fireEvent, render } from '@testing-library/react-native';
import React from 'react';
import { OrderButton } from '../order-button';

describe('OrderButton', () => {
  it('renders correctly with orderBy prop', () => {
    const { getByText } = render(<OrderButton orderBy='title' onSetOrder={() => {}} />);

    expect(getByText('Order by title')).toBeTruthy();
  });

  it('call onSetOrder on press', () => {
    const onSetOrderMock = jest.fn();
    const { getByText } = render(<OrderButton orderBy='date' onSetOrder={onSetOrderMock} />);

    fireEvent.press(getByText('Order by date'));
    expect(onSetOrderMock).toHaveBeenCalledWith('date');
  });
});
