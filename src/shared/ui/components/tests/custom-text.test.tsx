import { render } from '@testing-library/react-native';
import React from 'react';
import { CustomText } from '../custom-text';

describe('CustomText', () => {
  it('render the text', () => {
    const testText = 'test Text';
    const { getByText } = render(<CustomText>{testText}</CustomText>);

    expect(getByText(testText)).toBeTruthy();
  });

  it('apply styles correctly', () => {
    const { getByTestId } = render(
      <CustomText fontSize={20} fontWeight='bold' color='red'>
        Test
      </CustomText>
    );

    const textComponent = getByTestId('customText');
    expect(textComponent.props.style).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          fontSize: 20,
          fontWeight: 'bold',
          color: 'red',
        }),
      ])
    );
  });
});
