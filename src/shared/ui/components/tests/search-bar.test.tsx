import { fireEvent, render } from '@testing-library/react-native';
import React from 'react';
import { SearchBar } from '../search-bar';

describe('SearchBar', () => {
  const placeholderText = 'Search...';
  const initialQuery = '';

  it('renders correctly', () => {
    const { getByPlaceholderText } = render(<SearchBar searchQuery={initialQuery} setSearchQuery={() => {}} placeholder={placeholderText} />);

    expect(getByPlaceholderText(placeholderText)).toBeTruthy();
  });

  it('show searchQuery value', () => {
    const testQuery = 'test';
    const { getByDisplayValue } = render(<SearchBar searchQuery={testQuery} setSearchQuery={() => {}} placeholder={placeholderText} />);

    expect(getByDisplayValue(testQuery)).toBeTruthy();
  });

  it('calls setSearchQuery when text change', () => {
    const setSearchQueryMock = jest.fn();
    const { getByPlaceholderText } = render(<SearchBar searchQuery={initialQuery} setSearchQuery={setSearchQueryMock} placeholder={placeholderText} />);

    const newText = 'Test';
    fireEvent.changeText(getByPlaceholderText(placeholderText), newText);

    expect(setSearchQueryMock).toHaveBeenCalledWith(newText);
  });
});
