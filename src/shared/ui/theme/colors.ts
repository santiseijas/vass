export const colors = {
  primary: '#A2D2FF',
  background: '#FFFFFF',
  textSecondary: '#666666',
  border: '#CCCCCC',
  error: '#FFC4C4',
};
