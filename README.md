
# Guía de Inicio Rápido para el Proyecto Vass

Bienvenido a la documentación del proyecto Vass. Lo primero de todo quería agradecerte por leer esto y tomar tu tiempo para ver este proyecto. A continuación te dejo los pasos para instalar las dependencias, poder correr el proyecto, lanzar los tests, la arquitectura y los patrones de diseño utilizados y una breve explicación de la interfaz de usuario.

## Índice

1. [Requisitos Previos](#requisitos-previos)
2. [Instalación](#-instalación)
3. [Pruebas](#-pruebas)
4. [Arquitectura y Diseño](#-arquitectura-y-diseño)
5. [Interfaz de Usuario](#-interfaz-de-usuario)

## Requisitos Previos

- **Node.js >= 18**: Usa `$ nvm install` con `.nvmrc` o `$ nvm use` si ya está instalado.

## 🛠️ Instalación

1. **Clonar Repositorio**: `$ git clone git@gitlab.com:santiseijas/vass.git` o vía SSH.
2. **Instalar Paquetes**: En el directorio `vass/`, ejecutar `$ npm install`.
3. **Iniciar Aplicación**: `$ npx expo start --ios` para iOS o `$ npx expo start --android` para Android.

## 🧪 Pruebas

- Ejecutar `$ npm test` para lanzar la suite de pruebas.

## 🏗️ Arquitectura y Diseño

- **Arquitectura Hexagonal**: Con capas de dominio, aplicación, infraestructura y UI para separación de responsabilidades.
- **Hooks y Custom Hooks**: Uso extensivo para manejo de estado y lógica reutilizable.
- **Principios DRY y SOLID**: Adoptados para evitar repetición y mejorar la mantenibilidad.
- **Tests**: Estrategia integral con pruebas unitarias.
- **Alias de Importación**: Configurados para simplificar las referencias a módulos y componentes.

## 📱 Interfaz de Usuario



#### Barra de Búsqueda

- Ubicada en la parte superior de la interfaz, permite a los usuarios buscar artículos por título.

#### Botones de Ordenación

- ***Ordenar por Título**: Permite a los usuarios reorganizar los artículos alfabéticamente según su título.
- **Ordenar por Fecha**: Esta opción, que es la predeterminada, organiza los artículos mostrando primero los más recientes.

#### Lista de Artículos

- Los artículos se presentan en una lista, ordenados inicialmente por fecha para destacar los más nuevos. Cada entrada muestra el título del artículo, y una breve descripción.

#### Detalle del Artículo

Al seleccionar un artículo, los usuarios son llevados a una página de detalle que incluye:

- **Imagen**
- **Título**
- **Autor y Fecha**
- **Enlace para leer la noticia entera**
- **Descripción**


