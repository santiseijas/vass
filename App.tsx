import { Article } from '@news-domain';
import { Details, Main } from '@news-ui';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Header } from '@shared-components';
import { colors } from '@shared-theme';

export type RootStackParamList = {
  Main: undefined;
  Details: { item: Article };
};
export default function App() {
  const Stack = createNativeStackNavigator<RootStackParamList>();

  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerTitle: () => <Header />,

          headerShadowVisible: false,
          contentStyle: {
            backgroundColor: colors.background,
          },
        }}
      >
        <Stack.Screen name='Main' component={Main} />
        <Stack.Screen name='Details' component={Details} options={{ headerBackVisible: true, headerBackTitleVisible: false }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
