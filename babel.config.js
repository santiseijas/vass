module.exports = function (api) {
  api.cache(true);
  return {
    presets: ['babel-preset-expo'],
    presets: ['module:metro-react-native-babel-preset'],
    plugins: [
      [
        'module-resolver',
        {
          root: ['./src'],
          alias: {
            src: './src',
            '@news-domain': './src/news/domain/index.ts',
            '@news-application': './src/news/application/index.ts',
            '@news-infraestructure': './src/news/infraestructure/index.ts',
            '@news-ui': './src/news/ui/index.ts',
            '@shared-hooks': './src/shared/ui/hooks/index.ts',
            '@shared-components': './src/shared/ui/components/index.ts',
            '@shared-theme': './src/shared/ui/theme/index.ts',
            '@shared-utils': './src/shared/utils/index.ts',
          },
        },
      ],
    ],
  };
};
